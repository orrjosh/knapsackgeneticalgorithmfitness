function z = knapsack_fitness( x, weights, values, maximumWeight)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
    curWeight=0
    curValue=0
    for i=1:20
        bit=x(i)
        if bit==1
            curWeight=curWeight+weights(i)
            curValue=curValue+values(i)
        end
    end
    if curWeight>maximumWeight
        z=10000
    else
        z=-(curValue)
    end
end

